# saCMM self-adaptive Cooperative Multimethod library  #

## self-adaptive Cooperative Multimethod (saCMM)##

This is version 0.1 of a novel parallel global optimization code, self-adaptive 
cooperative multimethod (saCMM). This code is distributed as a library with several parallel solvers based on the scatter search and differential evolution metaheuristics, incorporating several key new mechanisms: 

1. Asynchronous cooperation between parallel processes.
2. Coarse and fine-grained parallelism.
3. Self-tuning strategies.
4. Concurrent multimethods runs.

The saCMM code has been implemented using Fortran 90 and C. Parallelization has been implemented using MPI and openMP. It has been tested in Linux clusters running CentOS 6.7.

The saCMM library allows the solution of non-linear programming (NLP) problems. It also provides efficient local solvers for nonlinear parameter estimation problems associated with complex models (e.g. those described by differential equations). The current distribution of saCMM includes a set of optimization examples that can be used as benchmarks, taken from the BBOB and BioPreDyn testbeds.

For the reproduction of the results presented in the main paper, we recommend using example scripts located in the folder reproducibility_scripts.

## REFERENCES ##

### Main reference: ###

P. Gonzalez, D.R. Penas, X.C. Pardo, J.R. Banga and R. Doallo  Multimethod optimization in the Cloud: a case-study in Systems Biology modelling. Concurrency and Computation - Practice and Experience. (Accepted)

### Related previous papers: ###

Penas, D.R., P. Gonzalez, J.A. Egea, R. Doallo and J.R. Banga (2017) Parameter estimation in large-scale systems biology models: a parallel and self-adaptive cooperative strategy. BMC Bioinformatics 18:52.

Penas DR, P Gonzalez, JA Egea, JR Banga, R Doallo (2015) Parallel Metaheuristics in Computational Biology: An Asynchronous Cooperative Enhanced Scatter Search Method. Procedia Computer Science, 51:630-639.

### Benchmark problems distributed with this library: ###

Villaverde AF, D Henriques, K Smallbone, S Bongard, J Schmid, D Cicin-Sain, A Crombach, J Saez-Rodriguez, K Mauch, E Balsa-Canto, P Mendes, J Jaeger and JR Banga (2015) BioPreDyn-bench: a suite of benchmark problems for dynamic modelling in systems biology. BMC Systems Biology 9:8.
